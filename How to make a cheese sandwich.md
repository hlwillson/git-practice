### How to make a Cheese Sandwich

#### Ingredients and Tools needed:
- Bread
- Cheese
- Butter
- Bacon (pre-cooked)
- Jalapeno
- Knife
- Stove
- Griddle
- Spatula
- Plate
- Tomato Soup
- Bowl
- Spoon

#### Steps to make a Cheese Sandwich:
1.  Preheat griddle on medium heat.
+ Butter bread slices.
+ Put 1 buttered bread slice in preheated griddle.
+ Add cheese slices.
+ Add pre-cooked bacon and jalapenos.
+ Place second buttered bread slice on top of cheese.
+ Grill until golden brown, about 2 minutes.
+ Flip sandwich and grill until golden brown, about 2 minutes.
+ Place on plate and serve with (heated) tomato soup.
+ Serve and enjoy!
